### Table of Contents

- [Table of Contents](#table-of-contents)
- [About](#about)
    + [Features](#features)
- [Requirements](#requirements)
  * [Node](#node)
  * [Node Package Manager or Yarn](#node-package-manager-or-yarn)
  * [Dependancies](#dependancies)
- [Directory layout](#directory-layout)
- [Deployment Instructions](#deployment-instructions)
- [Testing](#testing)
- [Home Page](#home-page)
- [Searching for Bus Stop Information](#searching-for-bus-stop-information)
- [Viewing Route Information](#viewing-route-information)
- [About Page](#about-page)
- [Help Page](#help-page)

### About


ReactJS real-time bus information app for Dublin city centre using the Data.gov.ie dataset.

##### Features

1. An index page with a welcome message presented in a modal window;
2. The main purpose of the app is to allow the user to input a bus stop number to receive real-time information detailing the next arriving bus at that stop. 
	            

![](demo/0_home.png)

### Requirements

#### Node

* https://nodejs.org/en/download/
* Tested on 64bit node v9.8.0

#### Node Package Manager or Yarn

* https://www.npmjs.com/get-npm
* https://yarnpkg.com/lang/en/docs/install
* Tested on 64bit npm v5.6.0 and yarn v1.5.1

#### Dependancies

* see react-bus-info-app/package.json

```
{
  "name": "react-bus-info-app",
  "version": "1.0.0",
  "private": true,
  "dependencies": {
    "axios": "^0.18.0",
    "bootstrap": "^4.1.1",
    "moment": "^2.22.2",
    "react": "^16.4.0",
    "react-dom": "^16.4.0",
    "react-flip-move": "^3.0.2",
    "react-router-dom": "^4.2.2",
    "react-scripts": "1.1.4",
    "reactstrap": "^6.0.1"
  },
  "scripts": {
    "start": "react-scripts start",
    "build": "react-scripts build",
    "test": "react-scripts test --env=jsdom",
    "eject": "react-scripts eject"
  }
}
```

### Directory layout
```
react-bus-info-app # www_root
.
├── build
│   ├── asset-manifest.json
│   ├── favicon.ico
│   ├── index.html
│   ├── manifest.json
│   ├── service-worker.js
│   └── static
│       ├── css
│       │   ├── main.cd9bfd09.css
│       │   └── main.cd9bfd09.css.map
│       ├── js
│       │   ├── main.b334c7c6.js
│       │   └── main.b334c7c6.js.map
│       └── media
│           ├── SmartDublinLogo.2d5ce9f4.svg
│           └── background-shutterstock_183698861-31e823f9d5d9.31e823f9.jpg
├── package-lock.json
├── package.json
├── public
│   ├── favicon.ico
│   ├── index.html
│   └── manifest.json
├── src
│   ├── components
│   │   ├── App.js
│   │   ├── nav
│   │   │   ├── Menu.css
│   │   │   └── Menu.js
│   │   ├── pages
│   │   │   ├── About.js
│   │   │   ├── Footer.js
│   │   │   ├── Header.js
│   │   │   └── Welcome.js
│   │   ├── requests
│   │   │   ├── RequestBusStopNumber.js
│   │   │   └── RequestRouteNumber.js
│   │   ├── results
│   │   │   ├── BusStopNumberResultItems.js
│   │   │   ├── BusStopNumberResultList.css
│   │   │   └── BusStopNumberResultList.js
│   │   └── tests
│   │       └── RequestBusStopNumber.test.js
│   ├── img
│   │   ├── SmartDublinLogo.svg
│   │   └── background-shutterstock_183698861-31e823f9d5d9.jpg
│   ├── index.css
│   ├── index.js
│   ├── registerServiceWorker.js
│   └── routes.js
└── yarn.lock

14 directories, 36 files
```
### Deployment Instructions

* Install dependancies by accessing the react-bus-info-app dir in a command shell and executing ```node install``` or ```yarn install```

```
react-bus-info-app ▶ yarn install
yarn install v1.5.1
info No lockfile found.
[1/4] 🔍  Resolving packages...
[2/4] 🚚  Fetching packages...
[3/4] 🔗  Linking dependencies...
[4/4] 📃  Building fresh packages...
success Saved lockfile.
✨  Done in 17.33s.
```

* Create a production build by accessing the react-bus-info-app dir in a command shell and executing ```node build``` or ```yarn build```

```
react-bus-info-app ▶ yarn build
yarn run v1.5.1
$ react-scripts build
Creating an optimized production build...
Compiled successfully.

File sizes after gzip:

  102.64 KB (+124 B)  build/static/js/main.b334c7c6.js
  21.46 KB (+925 B)   build/static/css/main.cd9bfd09.css
```

* Start the web server in development mode by accessing the react-bus-info-app dir in a command shell and executing ```node start``` or ```yarn start```


```
react-bus-info-app ▶ yarn start
Compiled successfully!

You can now view react-bus-info-app in the browser.

  Local:            http://localhost:3000/
  On Your Network:  http://192.168.2.205:3000/

Note that the development build is not optimized.
To create a production build, use yarn build.

```

* Alternatively start the web server in production mode accessing the react-bus-info-app dir in a command shell and executing ```serve -s build```

```
react-bus-info-app ▶ serve -s build
INFO: Accepting connections at http://localhost:3000
```

### Testing

* Execute the RequestBusStopNumber.test.js by accessing the react-bus-info-app dir in a command shell and executing ```node test``` or ```yarn test```

```
react-bus-info-app ▶ yarn test
 PASS  src/components/tests/RequestBusStopNumber.test.js
  ✓ renders without crashing (32ms)

Test Suites: 1 passed, 1 total
Tests:       1 passed, 1 total
Snapshots:   0 total
Time:        1.344s
Ran all test suites related to changed files.

Watch Usage
 › Press a to run all tests.
 › Press p to filter by a filename regex pattern.
 › Press t to filter by a test name regex pattern.
 › Press q to quit watch mode.
 › Press Enter to trigger a test run.
✨  Done in 5.56s.
```

### Home Page

* Tap on the Smart Dublin Icon in the Navbar
![](demo/0_home.png)

### Searching for Bus Stop Information

* Tap on the Home Page and "Get Started" button or tap Search in the Navbar and in the  "Bus Stop ID..." input box, enter a stopid.
![](demo/1_search.png)
* Remove results by tapping on them (the table color will change from white to pink)
![](demo/1_search_remove.png)
        
### Viewing Route Information

* Search for a Bus Stop ID, then tap on the 'RouteID' table result (e.g: 44) link on the left.
![](demo/2_route_info.png)


### About Page

* Tap on About in the Navbar
![](demo/3_help.png)

### Help Page

* Tap on Help in the Navbar
![](demo/3_help.png)
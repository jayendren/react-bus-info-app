/*
Assignment: BSc Mobile Web Applications, Digital Skills Academy
Student ID: STU-00001576
File created: 2018/06/05
Reference Sources: None

@author: Jayendren Maduray
@version: v1.0
-
Changelog: None
*/
import React, { Component } from 'react';
import Routes from '../routes';
import Menu from './nav/Menu';
import Footer from './pages/Footer';

class App extends Component {

	render() {
		return (
			<div>
				<Menu />	 			
	 			<Routes />
	 			<Footer />
	 		</div>
	   );
   }
}
export default App;
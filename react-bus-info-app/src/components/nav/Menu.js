/*
Assignment: BSc Mobile Web Applications, Digital Skills Academy
Student ID: STU-00001576
File created: 2018/06/05
Reference Sources: None

@author: Jayendren Maduray
@version: v1.0
-
Changelog: None
*/
import React, { Component } from 'react';
import './Menu.css';
import logo from '../../img/SmartDublinLogo.svg';
// Code reuse: reactrap navbar component in reactjs
// Ref: https://reactstrap.github.io/components/navbar/
import {
	Collapse,
	Navbar,
	NavbarToggler,
	NavbarBrand,
	Nav,
	NavItem,
	NavLink,
} from 'reactstrap';

class Menu extends Component {
	constructor(props) {
		super(props);

		this.toggle = this.toggle.bind(this);
		this.state = {
			isOpen: false
		};
	}

	toggle() {
		this.setState({
			isOpen: !this.state.isOpen
		});
	}

	render() {
		return (
			<div>
				<Navbar className="fixed-top" color="light" light expand="md">
					<NavbarToggler onClick={this.toggle} />
					<NavbarBrand className="mr-auto" href="/">
						<img className="Menu-logo" src={logo} alt="logo" text="Home"/>					
					</NavbarBrand>
					<Collapse isOpen={this.state.isOpen} navbar>
						<Nav className="ml-auto" navbar>
							<NavItem>
								<NavLink href="/search">Search</NavLink>
							</NavItem>						
							<NavItem>
								<NavLink href="/about">About</NavLink>
							</NavItem>
							<NavItem>
								<NavLink href="/help">Help</NavLink>
							</NavItem>							
						</Nav>
					</Collapse>
				</Navbar>
			</div>
		);
	}
}

export default Menu;
/*
Assignment: BSc Mobile Web Applications, Digital Skills Academy
Student ID: STU-00001576
File created: 2018/06/05
Reference Sources: https://reactstrap.github.io/

@author: Jayendren Maduray
@version: v1.0
-
Changelog: None
*/
import React, { Component } from 'react';
import { Container, Row, Col } from 'reactstrap';
import Header from '../pages/Header';

class About extends Component {
	constructor(props) {
    	super(props);
    	this.state = { 
    		modal: 		true,
			appName:    "City Centre real-time Bus Information",
			appHeading: "About"    		
    	};
  	}

	render() {
    	return (
      		<div>
      			<Header title={this.state.appName} heading={this.state.appHeading}/>
				<Container>
					<Row>
						<Col>
							<h3>1. Assessment Brief in detail</h3>
							<p>You are asked to build a real-time bus information app for Dublin city centre using the Data.gov.ie dataset.</p>
							<p>You are free to use any library or framework that you have studied on this module. Your solution must include the following features:</p>
							<ol>
								<li className="list">An index page with a welcome message presented in a modal window;</li>
								<li className="list">The main purpose of your app is to allow the user to input a bus stop number to receive real-time information detailing the next arriving bus at that stop. Examine the API documentation to see what information is available, and would be useful to the user in this context.</li>
							</ol>								
							<h3>Data Set Information</h3>
							<p>https://data.gov.ie/dataset/real-time-passenger-information-rtpi-for-dublin-bus-bus-eireann-luas-and-irish-rail/resource/4b9f2c4f-6bf5-4958-a43a-f12dab04cf61?inner_span=True</p>
							<ul><p>Example request format:</p>
							<li className="list">http://[rtpiserver]/realtimebusinformation?stopid=[stopid]&routeid=[routeid]&maxresults&operator=[operator]&format=[format]</li>
							</ul>								
							<ul><p>Example request:</p>
							<li className="list">https://data.smartdublin.ie/cgi-bin/rtpi/realtimebusinformation?stopid=7602&format=json</li>
							</ul>
							<h3>2. Next Steps</h3>
							<p>Use this value (bus stop number) to format a request to the API.</p>
							<p>Handle the response. Note that this may include an error code. </p>
							<p>Reference the documentation for details.</p>
							<p>Display the latest data to the user.</p>
							<h3>3. Essay</h3>
							<p>You are asked to include a two-part essay in pdf format, which will be included within your zip file submission.</p>
							<p>Part 1: A 500 word Rationale Essay that explains why you have chosen the libraries and/or framework in your solution.</p> 
							<p>You should consider:</p>
							<ol>
								<li className="list">what benefits your choice brings,</li>
								<li className="list">any constraints that your choice introduces,</li>
								<li className="list">what improvements you would make to the app in a future version.</li>
							</ol>	
							<p>Part 2: 300-word 'Extra Feature' Essay. You are asked only to describe this feature, but are free to develop it if you wish.</p>
							<p>Describe the process that you would need to use to auto-suggest the nearest stop to the user. </p>
							<p>Note that geo-location data is not included in the original data set, so you will need to consider other elements here. </p>
							<p>For example, a combination of search history and location data might work. </p>
							<p>Or, you might like to identify another data set to support the RTPI set and describe how you would use them together.</p>							
						</Col>
					</Row>
				</Container>
      		</div>
    	);
  	}
}

export default About
/*
Assignment: BSc Mobile Web Applications, Digital Skills Academy
Student ID: STU-00001576
File created: 2018/06/05
Reference Sources: None

@author: Jayendren Maduray
@version: v1.0
-
Changelog: None
*/
import React, { Component } from 'react';
import { Container, Row } from 'reactstrap';
// Code reuse: pretty formatted date/time - used to print the current year in the footer
// Ref: https://momentjs.com
import moment from 'moment';

class Footer extends Component {
	constructor(props) {
		super(props);

		this.state = {
			title: ""
		};
	}

	render() {
		return (
			<div>			
				<Container>	
					<Row>												
						<footer>
							<p>&copy; {moment().format('YYYY')} {this.props.title}</p>
						</footer>
					</Row>
				</Container>
			</div>
		);
	}
}

export default Footer
/*
Assignment: BSc Mobile Web Applications, Digital Skills Academy
Student ID: STU-00001576
File created: 2018/06/05
Reference Sources: https://reactstrap.github.io/components/layout/#app

@author: Jayendren Maduray
@version: v1.0
-
Changelog: None
*/
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Container, Row, Col } from 'reactstrap';

class Header extends Component {
	static propTypes = {
		title:   PropTypes.string.isRequired,
		heading: PropTypes.string.isRequired
	}	

	constructor(props) {
    	super(props);

    	this.state = {
      		title: "",
      		heading: ""
    	};
  	}

	render() {
    	return (
      		<div>
				<Container fluid>
					<Row>
						<Col>								
							<h1 className="display-5">{this.props.title}</h1>
							<h2>{this.props.heading}</h2>
						</Col>
					</Row>
				</Container>
      		</div>
    	);
  	}
}

export default Header
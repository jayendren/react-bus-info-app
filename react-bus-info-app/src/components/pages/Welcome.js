/*
Assignment: BSc Mobile Web Applications, Digital Skills Academy
Student ID: STU-00001576
File created: 2018/06/05
Reference Sources: https://appdividend.com/2018/03/30/react-bootstrap-modal-example-tutorial/

@author: Jayendren Maduray
@version: v1.0
-
Changelog: None
*/
import React from 'react';
// Code reuse: bootstrap modal component in reactjs
// Ref: https://reactstrap.github.io/components/modals/
import { 
	Button, 
	Modal, 
	ModalHeader, 
	ModalBody, 
	ModalFooter 
} from 'reactstrap';
import Header from '../pages/Header';

class Welcome extends React.Component {
	constructor(props) {
    	super(props);
    	this.state = { 
    		modal: 		true,
			appName:    "City Centre real-time Bus Information",
			appHeading: "Welcome"    		
    	};

    	this.toggle = this.toggle.bind(this);
    	this.handleSubmit = this.handleSubmit.bind(this);
  	}

	toggle() {
    	this.setState({
      		modal: !this.state.modal
    	});
  	}

  	handleSubmit(event) {
    	event.preventDefault();
  	}

  	render() {
    	return (

			<div>
				<Header title={this.state.appName} heading={this.state.appHeading}/>			
	        	<Modal centered isOpen={this.state.modal}>
		        	<form onSubmit={this.handleSubmit}>
			          	<ModalHeader>Dublin City Centre Real-time Bus Information</ModalHeader>
			          	<ModalBody>
			          		<div className="row">
			          			<div className="form-group col-xs-6 col-md-10">
			             			<i>Welcome to the real-time bus information app for Dublin City Centre using the data.gov.ie dataset.</i>
			             			<hr/>
			             			<i>You can input a bus stop number to receive real-time information detailing the next arriving bus at that stop.</i>
			             			<hr/>
			             			<i>Remove previous results by tapping on them, or filter results by tapping on the returned RouteID.</i>
			               		</div>
			              </div>
			          	</ModalBody>
			          	<ModalFooter>
			            	<Button href="/search" color="success" onClick={this.toggle}>Get Started</Button>
			          	</ModalFooter>
		          	</form>
	       		</Modal>
	        </div>      
    	);
  	}
}

export default Welcome;
/*
Assignment: BSc Mobile Web Applications, Digital Skills Academy
Student ID: STU-00001576
File created: 2018/06/05
Reference Sources: 
	https://daveceddia.com/ajax-requests-in-react/
	https://thinkster.io/tutorials/iterating-and-rendering-loops-in-react
	https://hackernoon.com/tutorial-how-to-make-http-requests-in-react-part-3-daa6b31b66be
	https://laracasts.com/discuss/channels/general-discussion/how-to-render-nested-json-respose-in-react
	https://codepen.io/chriscoyier/pen/jqyWXo
	https://daveceddia.com/ajax-requests-in-react/	
@author: Jayendren Maduray
@version: v1.0
-
Changelog: None
*/
import React, { Component } from 'react';
//import PropTypes from 'prop-types';
// Code Reuse: reactstrap alerts
// Ref: https://reactstrap.github.io/components/alerts/
import { Alert, Table } from 'reactstrap';
// Code Reuse: JSON parsing in reactjs
// Ref: https://github.com/axios/axios
import axios from 'axios';
import '../results/BusStopNumberResultList.css';
import Header from '../pages/Header';


class RequestRouteNumber extends Component {
	// Set up the default state with a property intitializer
	// instead of writing a whole constructor just for this
	// You can reference 'this.props' here if you need to.
	state = {
		response: [],
		loading:    true,
		error:      null,
		stopid:     this.props.match.params.stopid,
		routeid:    this.props.match.params.routeid,
		appName:    "City Centre real-time Bus Information",
		appHeading: "Route Info"
	}

	componentDidMount() {	
		axios.get(`https://data.smartdublin.ie/cgi-bin/rtpi/realtimebusinformation?stopid=${this.state.stopid}&routeid=${this.props.match.params.routeid}&format=json`)		
			.then(res => {
				// Transform the raw data by extracting the nested response
				const response   = res.data;				
				const error_code = response.errorcode;				
				// Code reuse: check if response returned error code > 0
				// error code map
				// 0 = success
				// 1 = no results
				// 2 = missing parameter
				// 3 = invalid parameter
				// 4 = scheduled downtime
				// 5 = unexpected system error
				// Ref: 
				// https://reactjs.org/docs/conditional-rendering.html				
				// https://react-cn.github.io/react/tips/if-else-in-JSX.html				
				var  error_result = '';
				switch(error_code){
					case "1":
						error_result = "no results";
						break;
					case "2":
						error_result = "missing parameter";
						break;
					case "3":
						error_result = "invalid parameter";
						break;
					case "4":
						error_result = "scheduled downtime";
						break;
					case "5":
						error_result = "unexpected system error";																								
						break;
					default:
						error_result = null;
						break;
				}																
				if (error_code > 0) {
					this.setState({
						loading: false,
						error:   error_result
					});
				} else {
					// Update state to trigger a re-render.
					// Clear any errors, and turn off the loading indiciator.
					this.setState({
						response,
						loading: false,
						error:   null
					});					
				}
			})
			.catch(err => {
				// Something went wrong. Save the error in state and re-render.
				this.setState({
					loading: false,
					error:   err
				});
			});
	}

	renderLoading() {
		return (<div><h4>Loading...</h4></div>);
	}

	renderError() {
		return (
			<div>
				<Alert color="danger">
					ERROR - API returned: {this.state.error}
				</Alert>
			</div>
		);
	}

	renderResults() {
	// Using destructuring to extract the 'error' and 'results'
	// keys from state. This saves having to write "this.state.X" everwhere.
		const { error, response } = this.state;

		if(error) {
			return this.renderError();
		}		
		var html = response.results.map(result =>				
			(<div key={result.arrivaldatetime}>	
				<Table responsive striped bordered hover size="sm">
					<thead>
				    	<tr>	
				    		<th>Operator</th>			  
				    		<th>Direction</th>    		
				      		<th>Origin</th>				      		
				      		<th>Arrival</th>
				      		<th>Departure</th>
				      		<th>Destination</th>				      		
				    	</tr>
				  	</thead>
				  	<tbody>
				    	<tr>	
				    		<td>{result.operator}</td>
				    		<td>{result.direction}</td>	
				      		<td>{result.origin}</td>
				      		<td>{result.scheduledarrivaldatetime}</td>
				      		<td>{result.departuredatetime}</td>
				      		<td>{result.destination}</td>				      		
				    	</tr>
				  	</tbody>
				</Table>
			</div>)
		)	
		return (
			<div> 
				<Header title={this.state.appName} heading={this.state.appHeading}/>
				<div className="ResultListMain">
					<ul className="theList">
						<div className="resTable">
							<h5>{`StopID: ${this.state.stopid} | RouteID: ${this.state.routeid}`}</h5>
							{html}
						</div>
					</ul>
				</div>
			</div>
		);
	}

	render() {	
		const { loading } = this.state;
		return (
			<div>				
				{loading ? this.renderLoading() : this.renderResults()}
			</div>
		);
	}
}

export default RequestRouteNumber
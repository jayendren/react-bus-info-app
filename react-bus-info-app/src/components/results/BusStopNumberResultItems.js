/*
Assignment: BSc Mobile Web Applications, Digital Skills Academy
Student ID: STU-00001576
File created: 2018/06/05
Reference Sources: https://www.kirupa.com/react/simple_todo_app_react.htm

@author: Jayendren Maduray
@version: v1.0
-
Changelog: None
*/
import React, { Component } from "react";
// Code reuse: animated deletion of results
// Ref: https://github.com/joshwcomeau/react-flip-move
import FlipMove from "react-flip-move";
import "./BusStopNumberResultList.css";
import RequestBusStopNumber from "../requests//RequestBusStopNumber";

// Form to accept bus stop id
// Displays results and keeps a history of previous searches
// which can be deleted by clicking on them
class BusStopNumberResultItems extends Component {
	constructor(props) {
		super(props);
 
		this.createTasks = this.createTasks.bind(this);
	}
 
	delete(key) {
		this.props.delete(key);
	}
	// Code reuse: workaround for 
	// Warning: validateDOMnesting(...): <div> cannot appear as a descendant of <p>
	// Ref: https://stackoverflow.com/questions/41928567/div-cannot-appear-as-a-descendant-of-p
	createTasks(item) {
		return (
			<div className="resTable" onClick={() => this.delete(item.key)} key={item.key}>
				{item.text && <RequestBusStopNumber stopid={item.text} />}
			</div>
		)
	}
	// Code reuse: reverse list order as new results are displayed
	// Ref: https://stackoverflow.com/questions/37664041/react-given-an-array-render-the-elements-in-reverse-order-efficiently 
	render() {
		var ResultEntries = this.props.entries;
		var listItems     = ResultEntries.map(this.createTasks);
 
		return (
			<ul className="theList">
				<FlipMove duration={250} easing="ease-out">
					{listItems.reverse()}
				</FlipMove>
			</ul>
		);
	}
};
 
export default BusStopNumberResultItems;
/*
Assignment: BSc Mobile Web Applications, Digital Skills Academy
Student ID: STU-00001576
File created: 2018/06/05
Reference Sources: https://www.kirupa.com/react/simple_todo_app_react.htm

@author: Jayendren Maduray
@version: v1.0
-
Changelog: None
*/
import React, { Component } from 'react';
// Code reuse: reactsrap button
// Ref: https://reactstrap.github.io/components/buttons/
import { Form, Button } from 'reactstrap';
import BusStopNumberResultItems from './BusStopNumberResultItems';
import Header from '../pages/Header';

class ResultList extends Component {
	constructor(props) {
		super(props);

		this.state = {
			items: 		[],
			appName:    "City Centre real-time Bus Information",
			appHeading: "Search"			
		};
	
		this.addItem    = this.addItem.bind(this);
		this.deleteItem = this.deleteItem.bind(this);
	}

	addItem(e) {
		if (this._inputElement.value !== "") {
			var newItem = {
				text: this._inputElement.value,
				key:  Date.now()
			};
	 
			this.setState((prevState) => {
				return { 
					items: prevState.items.concat(newItem) 
				};
			});
		 
			this._inputElement.value = "";
		}		 
			 
		e.preventDefault();
	}

	deleteItem(key) {
		var filteredItems = this.state.items.filter(function (item) {
			return (item.key !== key);
		});
	 
		this.setState({
			items: filteredItems
		});
	}

	render() {

		return (   
			<div>
				<Header title={this.state.appName} heading={this.state.appHeading}/>
				<div className="ResultListMain">				
					<div className="header">
						<Form onSubmit={this.addItem}>
							<input align="center" className="form-control" width="auto" ref={(a) => this._inputElement = a} 
								placeholder="Bus Stop ID, e.g: 7603">
							</input>
							<Button color="primary" type="submit">search</Button>
						</Form>
					</div>				

					<BusStopNumberResultItems 
						entries = {this.state.items}
						delete  = {this.deleteItem}
					/>
				</div>
			</div>
		);
	}
}
export default ResultList;
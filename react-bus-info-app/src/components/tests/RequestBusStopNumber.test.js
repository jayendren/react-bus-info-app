/*
Assignment: BSc Mobile Web Applications, Digital Skills Academy
Student ID: STU-00001576
File created: 2018/06/05
Reference Sources: None

@author: Jayendren Maduray
@version: v1.0
-
Changelog: None
*/
import React from 'react';
import ReactDOM from 'react-dom';
import RequestBusStopNumber from '../requests/RequestBusStopNumber';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<RequestBusStopNumber stopid="7603"/>, div);
  ReactDOM.unmountComponentAtNode(div);
});

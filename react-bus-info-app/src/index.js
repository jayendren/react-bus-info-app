/*
Assignment: BSc Mobile Web Applications, Digital Skills Academy
Student ID: STU-00001576
File created: 2018/06/05
Reference Sources: None

@author: Jayendren Maduray
@version: v1.0
-
Changelog: None
import './index.css';
*/
import React from 'react';
import ReactDOM from 'react-dom';
import App from "./components/App";
import 'bootstrap/dist/css/bootstrap.css';
import './index.css';

ReactDOM.render(
    <div>
    	<App />
    </div>,
  	document.getElementById('root')
);

/*
Assignment: BSc Mobile Web Applications, Digital Skills Academy
Student ID: STU-00001576
File created: 2018/06/05
Reference Sources: 
https://www.9lessons.info/2017/10/reactjs-welcome-page-with-routing.html
https://stackoverflow.com/questions/37696391/multiple-params-with-react-router

@author: Jayendren Maduray
@version: v1.0
-
Changelog: None
*/
import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Welcome from './components/pages/Welcome';
import Search from './components/results/BusStopNumberResultList';
import About from './components/pages/About';
import Help from './components/pages/Welcome';
import RouteInfo from './components/requests/RequestRouteNumber';

const Routes = () => (
	<BrowserRouter >
		<Switch>
			<Route exact path="/" component={Welcome}/>
			<Route path="/search/:stopid?" component={Search}/>
			<Route path="/about" component={About}/>
			<Route path="/help" component={Help}/>
			<Route path="/routeinfo/:stopid/:routeid" component={RouteInfo}/>
		</Switch>
	</BrowserRouter>
);

export default Routes;